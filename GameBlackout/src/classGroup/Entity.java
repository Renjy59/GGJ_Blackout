package classGroup;

import processing.core.PApplet;

public class Entity {
	private int centerX, centerY;
	PApplet parent;

	public void initPApplet(PApplet p) {
		parent = p;
	}

	public Entity(int centerX, int centerY) {
		this.centerX = centerX;
		this.centerY = centerY;
	}

	// -------------Setters-----------------//
	public void setCenter(int x, int y) {
		this.centerX = x;
		this.centerY = y;
	}

	// -------------Getters-----------------//

	public int getCenterX() {
		return this.centerX;
	}

	public int getCenterY() {
		return this.centerY;
	}

}
