package classGroup;

import processing.core.PApplet;

public class Wave {
	private int centerX, centerY;
	private int size = 0;
	private int speed = 6;
	private int sizeMax;
	private int force;
	PApplet parent;

	public Wave(int centerX, int centerY, int sizeMax, PApplet p) {
		this.centerX = centerX;
		this.centerY = centerY;
		this.force = sizeMax;
		this.sizeMax = sizeMax;
		this.parent = p;
	}

	public void drawWave(PApplet p) {
		this.parent = p;
		parent.noFill();
		this.waveProgress();
		parent.stroke(255);
		parent.ellipse(this.centerX, this.centerY, this.size, this.size);
	}

	public void waveProgress() {
		if (size < sizeMax) {
			size += speed;
		} else {
			this.force = 0;
		}
	}

	public void reset(int centerX, int centerY, int sizeMax) {
		this.centerX = centerX;
		this.centerY = centerY;
		this.sizeMax = sizeMax;
		this.force = sizeMax;
		this.size = 0;
	}

	// -------------Setters-----------------//
	public void setCenter(int x, int y) {
		this.centerX = x;
		this.centerY = y;
	}

	public void setSizeMax(int sizeMax) {
		this.sizeMax = sizeMax;
	}

	// -------------Getters-----------------//

	public int getCenterX() {
		return this.centerX;
	}

	public int getCenterY() {
		return this.centerY;
	}

	public int getSize() {
		return this.size;
	}

	public int getForce() {
		return this.force;
	}
}
