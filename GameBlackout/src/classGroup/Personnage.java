package classGroup;

import java.util.ArrayList;

public class Personnage extends Entity {
	int speed, i, count = 0, indice = 0;
	ArrayList<Wave> waves = new ArrayList<Wave>();
	int millis_actu, millis_prece;

	final int FREQUENCE_WAVE = 1500; // tempo en milli secondes pour la
										// frequence des waves

	public Personnage(int x, int y, int speed) {
		super(x, y);
		this.speed = speed;
		for (i = 0; i < 5; i++) {
			waves.add(new Wave(0, 0, 0, super.parent));
		}
	}

	public void move(int x, int y) {
		setCenter(getCenterX() + speed * x, getCenterY() + speed * y);
	}

	public void drawPerso() {

		for (i = 0; i < 5; i++) {
			if (waves.get(i).getForce() > 0) { // Lance les ondes avec une force supérieur à 0
				waves.get(i).drawWave(parent);
			}

			millis_actu = super.parent.millis();
			  if (millis_actu-millis_prece >= FREQUENCE_WAVE){
			    millis_prece = millis_actu;
			    waves.get(indice).reset(getCenterX(),getCenterY(),400);
				if (indice < 4) {
					indice++;
				} else {
					indice = 0;
				}
			}

		}
		super.parent.fill(255);
		super.parent.ellipse(getCenterX(), getCenterY(), 5, 5);
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return this.speed;
	}
}