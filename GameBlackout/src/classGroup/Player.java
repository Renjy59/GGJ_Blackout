package classGroup;

public class Player extends Personnage {
	int deplacementX = 0, deplacementY = 0;
	int sprint = 1;

	public Player(int x, int y, int speed) {
		super(x, y, speed);
	}

	public void setVitesse(int x, int y) {
		this.deplacementX = x;
		this.deplacementY = y;
	}

	public void setSprint(boolean s) {
		if (s == true)
			this.sprint = 2;
		else
			this.sprint = 1;
	}

	public void move() {
		move(deplacementX * sprint, deplacementY * sprint);
	}

}