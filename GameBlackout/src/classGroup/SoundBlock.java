package classGroup;

public class SoundBlock extends Entity {
	String sound;

	public SoundBlock(int x, int y, String sound) {
		super(x, y);
		this.sound = sound;
	}

	public void setSound(String sound) {
		this.sound = sound;

	}

	public void playSound() {

	}
}
