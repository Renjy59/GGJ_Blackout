package processing;

import classGroup.*;
import processing.core.PApplet;

//Pour plus d'info : https://processing.org/tutorials/eclipse/
public class UsingProcessing extends PApplet {

	Player player = new Player(300, 300, 1);
	int deplacementX = 0, deplacementY = 0;

	public static void main(String[] args) {
		PApplet.main("processing.UsingProcessing");
	}

	public void settings() {
		size(1200, 800);
		player.initPApplet(this);
	}

	public void setup() {
		background(0);
	}

	public void draw() {
		background(0);
		player.setVitesse(deplacementX, deplacementY);
		player.drawPerso();
		player.move();
	}

	public void keyPressed() {
		switch (keyCode) {
		case LEFT: // GAUCHE
			deplacementX = -1;
			break;

		case RIGHT:// DROITE
			deplacementX = 1;
			break;

		case UP: // HAUT
			deplacementY = -1;
			break;

		case DOWN: // BAS
			deplacementY = 1;
			break;

		case SHIFT:
			player.setSprint(true);
			break;
		}
	}

	public void keyReleased() {
		switch (keyCode) {
		case LEFT: // GAUCHEs
			deplacementX = 0;
			break;

		case RIGHT:// DROITE
			deplacementX = 0;
			break;

		case UP: // HAUT
			deplacementY = 0;
			break;

		case DOWN: // BAS
			deplacementY = 0;
			break;

		case SHIFT:
			player.setSprint(false);
			break;
		}

	}
}